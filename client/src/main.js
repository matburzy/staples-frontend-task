import Vue from "vue";
import Toasted from "@gitlab/vue-toasted";
import BootstrapVue from "bootstrap-vue";
import App from "./App.vue";
import Axios from "axios";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { faSort } from "@fortawesome/free-solid-svg-icons";
import { faCartPlus } from "@fortawesome/free-solid-svg-icons";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { faDatabase } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
Vue.use(Axios);
Vue.use(BootstrapVue);
Vue.use(Toasted);
library.add(faSearch);
library.add(faSort);
library.add(faCartPlus);
library.add(faDatabase);
library.add(faTrash);
Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.config.productionTip = false;

new Vue({
  render: h => h(App)
}).$mount("#app");
